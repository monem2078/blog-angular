import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class RegisterService {

  constructor(
      private http: Http
  ) { }

    newUser(user) {
        const headers = new Headers();
        headers.append('Content-type' , 'application/json');
        return this.http.post('http://localhost:8000/api/auth/register', user, {headers : headers}).map(res => res.json());
    }


}
