import { Component, OnInit } from '@angular/core';
import {LoginService} from '../../Services/login/login.service';
import {User} from '../../Models/User';
import {Router} from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    user: User;
  constructor(
      private login: LoginService, private router: Router
  ) {
      this.user = new User();
  }

  ngOnInit() {
  }
    loginUser() {
        this.login.login(this.user).subscribe(data => {
            localStorage.setItem('token', data.token);
            this.router.navigateByUrl('register');
        });
    }
}
