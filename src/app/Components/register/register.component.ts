import { Component, OnInit } from '@angular/core';
import {RegisterService} from '../../Services/register/register.service';
import {User} from '../../Models/User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
   user: User;

  constructor(
      private register: RegisterService
  ) {
    this.user = new User();
  }

  ngOnInit() {
  }

  addUser() {
    this.register.newUser(this.user).subscribe();
  }

}
