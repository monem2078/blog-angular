import { RegisterComponent } from '../Components/register/register.component';
import { LoginComponent } from '../Components/login/login.component';

export const routes = [
    {
        path: '',
        redirectTo: 'register',
        pathMatch: 'full'
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    {
        path: 'login',
        component: LoginComponent
    }
];

export const navigatingComponents = [
    RegisterComponent,
    LoginComponent
];
