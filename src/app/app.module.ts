import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// Components
import { AppComponent } from './app.component';

// Services
import { RegisterService } from './Services/register/register.service';
import { LoginService } from './Services/login/login.service';

// Routes
import { routes, navigatingComponents } from './routing/routes';
import { LoginComponent } from './Components/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    ...navigatingComponents,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
      RegisterService,
      LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
